# User guide for sq

This is a sketch of a user guide for sq, the command line tool from
the Sequoia PGP project.

`sq` is a command line tool for encrypting and authenticating files
and messages. It is provided by the Sequoia PGP project as part of an
implementation of the OpenPGP standard in the Rust programming
language. This is a guide shows how to `sq`, and is aimed at people
comfortable on the command line, and explains any concepts of
cryptography needed to use `sq`.

- write as Markdown, render as website, and possibly EPUB and PDF,
  using mdbook and/or Pandoc and/or Subplot
  - also, probably include in the sq package somehow, eventually, so
    it's available to those working offline
  - however, keep it separate for now to avoid putting a useless
    burden on the Sequoia CI system
- possibly publish via print publisher as well, or explore
  print-on-demand self-publishing, but that is secondary, the primary
  goal is driving sq adoption by making it easier to use
- publish under a free content license - CC-BY-SA 4.0?
- rig up something to test command line examples in CI

The rendered version of this guide is available [in HTML](https://sequoia-pgp.gitlab.io/sq-user-guide/) and [in PDF](https://sequoia-pgp.gitlab.io/sq-user-guide/sq-guide.pdf).
